import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.IntStream;

public class main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        LoginService.doDirectLogin("direct1");

        LoginService.doBackgroundLogin("background1");

        LoginService.doDirectLogin("direct2 - queued");

        // submit asynchronously
        LoginService.doBackgroundLogin("background2 - skipped");

        Future<Boolean> faildirect3 = LoginService.doDirectLogin("faildirect3");
        Boolean result = faildirect3.get(); // wait for result synchronously
        System.out.println("faildirect3 login result: " + result);

        IntStream.range(1, 20).forEach(
                i -> {
                    try {
                        LoginService.doDirectLogin("overflowtest" + i);
                    } catch (Exception e) {
                    }
                }
        );

        LoginService.shutdown();
    }
}
