import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class LoginService {

    private static final int MAX_QUEUED_DIRECT_LOGINS = 10;
    private static final int N_THREADS = 2;
    private static ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(N_THREADS);

    public static Future<Boolean> doDirectLogin(String username) {
        if(executor.getQueue().size() <= MAX_QUEUED_DIRECT_LOGINS) {
            return executor.submit(() -> login(username));
        }
        System.out.println("Skipping direct login for user: " + username);
        throw new IllegalStateException("Queue reached max");
    }

    public static void doBackgroundLogin(String username) {
        if(executor.getQueue().isEmpty()) {
            executor.execute(() -> login(username));
        } else {
            System.out.println("Skipping background login for user: " + username);
        }
    }

    private static boolean login(String username) {
        System.out.println("Call remote service for user: " + username);
        try {
            Thread.sleep(200L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return username.startsWith("fail") ? false : true;
    }

    public static void shutdown() {
        executor.shutdown();
    }
}
